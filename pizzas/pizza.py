#!/usr/bin/env python3

import collections


class Pizza:
    def __init__(self, grid, min_num_ingredients, max_slice_size):
        self.grid = grid
        self.min_num_ingredients = min_num_ingredients
        self.max_slice_size = max_slice_size

    @property
    def columns(self):
        try:
            return len(self.grid[0])
        except IndexError:
            return 0

    @property
    def rows(self):
        return len(self.grid)

    def __str__(self):
        return "R:{} C:{} I:{} S:{}\n".format(
            self.rows, self.columns, self.min_num_ingredients,
            self.max_slice_size) + "\n".join("".join(line)
                                             for line in self.grid)

    __repr__ = __str__

    def load_file(filename):
        with open(filename) as f:
            f = map(lambda l: l.rstrip('\n\r'), f)
            rows, columns, min_num_ingredients, max_slice_size = [
                int(n) for n in next(f).split(" ")
            ]

            grid = [list(line) for line in f]

            return Pizza(grid, min_num_ingredients, max_slice_size)


PizzaSlice = collections.namedtuple(
    'PizzaSlice', ('start_row', 'start_column', 'end_row', 'end_column'))


def get_slice_size(slice):
    return (slice.end_row - slice.start_row + 1) * (
        slice.end_column - slice.start_column + 1)


class CutPizza:
    def __init__(self, pizza):
        self.pizza = pizza
        self.already_cut_grid = [[False] * pizza.columns
                                 for _ in range(pizza.rows)]
        self.slices = []

    def is_slice_cutable(self, slice):
        if not (slice.start_row < slice.end_row and
                slice.start_column < slice.end_column and slice.start_row >= 0
                and slice.end_row < self.pizza.rows and slice.start_column >= 0
                and slice.end_column < self.pizza.columns
                and get_slice_size(slice) <= self.pizza.max_slice_size):
            return False

        N_mushrooms = 0
        N_tomatoes = 0
        for i in range(slice.start_row, slice.end_row + 1):
            for j in range(slice.start_column, slice.end_column + 1):
                if self.already_cut_grid[i][j]:
                    return False

                if self.pizza.grid[i][j] == 'M':
                    N_mushrooms += 1
                else :
                    N_tomatoes += 1

        min_N = self.pizza.min_num_ingredients
        return N_mushrooms >= min_N and N_tomatoes >= min_N

    def add_slice(self, slice):
        if not self.is_slice_cutable(slice):
            return False

        for i in range(slice.start_row, slice.end_row + 1):
            for j in range(slice.start_column, slice.end_column + 1):
                if self.already_cut_grid:
                    self.already_cut_grid[i][j] = True

        self.slices.append(slice)
        return True

    def remove_slice(self, slice_num):
        if slice_num >= len(self.slices):
            return

        slice = self.slices[slice_num]
        for i in range(slice.start_row, slice.end_row + 1):
            for j in range(slice.start_column, slice.end_column + 1):
                if self.already_cut_grid:
                    self.already_cut_grid[i][j] = False

        del self.slices[slice_num]


    @property
    def num_slices(self):
        return len(self.slices)

    def __str__(self, collorize=False):
        cell_pre = '\x1b[32m' if collorize else ''
        cell_post = '\x1b[0m' if collorize else ''

        process_cell = lambda i, j, v: cell_pre + v + cell_post if self.already_cut_grid[i][j] else v.lower()
        process_row = lambda row: map(lambda cell: process_cell(row[0], cell[0], cell[1]), enumerate(row[1]))

        process_slice = lambda slice: "{} {} {} {}".format(slice.start_row, slice.start_column, slice.end_row, slice.end_column)

        return ("\n".join("".join(process_row(row))
                          for row in enumerate(self.pizza.grid)) + "\n\n" +
                "\n".join(map(process_slice, self.slices)))

    def __repr__(self):
        return self.__str__(True)

    def save_to_file(self, filename):
        with open(filename, 'w') as f:
            f.write("%d\n" % self.num_slices)

            for slice in self.slices:
                f.write("%d %d %d %d\n" % slice)

    def score(self):
        return sum(map(get_slice_size, self.slices))
