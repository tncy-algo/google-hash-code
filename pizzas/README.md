# Pizzas !

Il s'agit du sujet d'entraînement par défaut proposé par Google sur la plateforme du Judge System.  
Le PDF du sujet est disponible à la racine de ce répertoire.

![Image issue du sujet](https://i.imgur.com/biYSC9S.png "Découpes de pizzas. Seule la 3ème correspond à une pizza simulée.")

### Script de manipulation des pizzas

Le script Python `pizza.py` vous permet de créer des objets `Pizza`, de les découper et ainsi d'en faire des objets `CutPizza`.  
Il vous permet entre autre de charger un fichier de données d'entrée et de déterminer le score de votre découpe.
