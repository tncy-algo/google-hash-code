#!/usr/bin/python3

from sys import argv

if len(argv) < 3 :
    print("Usage : ./hashcode2018.py <dataset.in> <result.out>")
    exit(1)

# ~~~~~~~~~~~~~~ loading the input file ~~~~~~~~~~~~~~ #

infile = open(argv[1], "r")
lines = infile.read().split("\n")
lines.remove("")
args = lines.pop(0)
infile.close()

"""
According to the subject :
 * R : number of rows of the grid
 * C : number of columns of the grid
 * F : number of vehicles in the fleet
 * N : number of rides
 * B : per-ride bonus value
 * T : number of steps in the simulation
"""
R, C, F, N, B, T = map(int, args.split(" "))

rides = []
for line in lines :
    ride = {}
    ride["x1"], ride["y1"], ride["x2"], ride["y2"], ride["s"], ride["f"] = map(int, line.split(" "))
    rides.append(ride)

# ~~~~~~~~~~~~~~ loading the output file ~~~~~~~~~~~~~~ #

outfile = open(argv[2], "r")
lines = outfile.read().split("\n")
lines.remove("")
outfile.close()

vehicles = [list(map(int, line.split(" ")[1:])) for line in lines][:F]
"""
example for the code above :
  line = "4 8 5 2 3"
  line.split(" ") = ["4", "8", "5", "2", "3"]
  line.split(" ")[1:] = ["8", "5", "2", "3"]
  map(int, line.split(" ")[1:]) = <8, 5, 2, 3> (iterable object)
  list(map(int, line.split(" ")[1:])) = [8, 5, 2, 3] (list)

so an item of vehicle is simply a list of rides indexes.
"""

# ~~~~~~~~~~~~~~ computing the score ~~~~~~~~~~~~~~ #

distance = lambda xa, ya, xb, yb : abs(xb - xa) + abs(yb - ya)

score = 0

for v in vehicles :
    x, y = 0, 0
    t = 0
    for i in v :
        ride = rides[i]

        # Moving to start position
        t += distance(x, y, ride["x1"], ride["y1"])
        x, y = ride["x1"], ride["y1"]

        # If we arrive too early, we wait
        if t < ride["s"] : t = ride["s"]
        
        # If we start at the earliest step of the ride, we get bonus points !
        if t == ride["s"] : score += B

        # Moving to the end position
        t += distance(x, y, ride["x2"], ride["y2"])
        x, y = ride["x2"], ride["y2"]

        # If we finish before the latest step of the ride, we increase the score by the distance of the ride
        if t < ride["f"] : score += distance(ride["x1"], ride["y1"], ride["x2"], ride["y2"])

print(score)        
