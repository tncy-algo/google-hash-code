# Google Hash Code 2018

Le sujet de 2018 traite sur les véhicules autonomes.  
Le PDF du sujet est disponible à la racine de ce répertoire.

### Problème

On dispose de plusieurs véhicules pouvant se déplacer sur une grille à coordonnées entières et positives. Tous les véhicules sont initialement positionnés aux coordonnées (0,0).  
On réalise une simulation où, à chaque étape, chaque voiture peut se déplacer (ou non) d'une unité sur la grille. Les numéros des étapes permettent alors de définir une *date*.  
La simulation commence à la date 0. Cela signifie qu'à la date 1, les voitures peuvent se trouver à l'emplacement (0,0), (0,1) ou (1,0).  

Plusieurs passagers sur la grille ont besoin d'être déplacés d'un point A à un point B, et dans un intervalle de temps donné.  

### Objectif

![Image issue du sujet](https://i.imgur.com/70si6np.png "Voiture se déplaçant sur la grille")

Votre mission, si vous l'acceptez, est de déplacer un maximum de passagers en répartissant les trajets à toutes les voitures.  
Le score est déterminé par la somme des distances des trajets qui ont pu être effectués. Des points bonus s'ajoutent si vous commencez le trajet le plus tôt possible !  

### Script de détermination du score

Un script Python vous permet d'évaluer votre score en fonction d'un fichier de données `.in` et d'un autre fichier que vous aurez généré, contenant les résultats.  
Pour déterminer votre score :

```bash
./hashcode2018 <fichier de données> <fichier résultat>
```
